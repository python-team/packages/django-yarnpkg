************
Installation
************

Install `Yarn <https://yarnpkg.com/>`_ from npm:

.. code-block:: bash

    npm install -g yarn

And django-yarnpkg package:

.. code-block:: bash

    pip install django-yarnpkg

Add django-bower to `INSTALLED_APPS` in your settings:

.. code-block:: python

    'django_yarnpkg',

Add staticfinder to `STATICFILES_FINDERS`:

.. code-block:: python

    'django_yarnpkg.finders.NodeModulesFinder',

Specify path to node modules root (you need to use absolute path):

.. code-block:: python

    NODE_MODULES_ROOT = '/PROJECT_ROOT/node_modules/'

If you need, you can manually set path to yarn

.. code-block:: python

    YARN_PATH = '/usr/bin/yarnpkg'

Example settings file with django-yarnpkg:

.. code-block:: python
    :linenos:

    import os


    PROJECT_ROOT = os.path.abspath(
        os.path.join(os.path.dirname(__file__), ".."),
    )

    DEBUG = True
    TEMPLATE_DEBUG = DEBUG

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': 'db',
            'USER': '',
            'PASSWORD': '',
            'HOST': '',
            'PORT': '',
        }
    }

    STATIC_ROOT = os.path.join(PROJECT_ROOT, 'static')

    STATIC_URL = '/static/'

    NODE_MODULES_ROOT = os.path.join(PROJECT_ROOT, 'node_modules')

    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',
        'django_yarnpkg.finders.NodeModulesFinder',
    )

    SECRET_KEY = 'g^i##va1ewa5d-rw-mevzvx2^udt63@!xu$-&di^19t)5rbm!5'

    TEMPLATE_LOADERS = (
        'django.template.loaders.filesystem.Loader',
        'django.template.loaders.app_directories.Loader',
    )

    MIDDLEWARE_CLASSES = (
        'django.middleware.common.CommonMiddleware',
        'django.contrib.sessions.middleware.SessionMiddleware',
        'django.middleware.csrf.CsrfViewMiddleware',
        'django.contrib.auth.middleware.AuthenticationMiddleware',
        'django.contrib.messages.middleware.MessageMiddleware',
    )

    ROOT_URLCONF = 'example.urls'

    WSGI_APPLICATION = 'example.wsgi.application'

    TEMPLATE_DIRS = (
        os.path.join(PROJECT_ROOT, 'templates'),
    )

    INSTALLED_APPS = (
        'django.contrib.staticfiles',
        'django_yarnpkg',
    )

    YARN_INSTALLED_APPS = (
        'jquery',
        'underscore',
    )
