.. django-yarnpkg documentation master file, created by
   sphinx-quickstart on Tue Jul 16 16:38:23 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-yarnpkg's documentation!
========================================

Easy way to use `Yarn <https://yarnpkg.com/>`_ with your `django <https://www.djangoproject.com/>`_ project.

Yarn is a package manager for the web. It offers a generic, unopinionated solution to the problem of front-end package management, while exposing the package dependency model via an API that can be consumed by a more opinionated build stack. There are no system wide dependencies, no dependencies are shared between different apps, and the dependency tree is flat.

Yarn runs over Git, and is package-agnostic. A packaged module can be made up of any type of asset, and use any type of transport (e.g., AMD, CommonJS, etc.).

Contents:

.. toctree::
   :maxdepth: 2

   installation
   usage
   tests
   example

`Visit django-yarnpkg EduGit page. <https://edugit.org/AlekSIS/libs/django-yarnpkg>`_



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

