*************
Running tests
*************

For running tests you need to install `django-yarnpkg` in development mode with:

.. code-block:: bash

    python setup.py develop

Install dev requirements:

.. code-block:: bash

    pip install -r requirements_dev.txt

Now you can run tests with:

.. code-block:: bash

    django-admin.py test --settings=django_yarnpkg.test_settings django_yarnpkg

You can change test project root with `TEST_PROJECT_ROOT` environment variable. By default it is `/tmp`.
