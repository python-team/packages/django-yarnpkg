*****
Usage
*****

Specify `YARN_INSTALLED_APPS` in settings, like:

.. code-block:: python

    YARN_INSTALLED_APPS = (
        'jquery#1.9',
        'underscore',
    )

Download yarn packages with management command:

.. code-block:: bash

    ./manage.py yarn install

Add scripts in template, like:

.. code-block:: html+django

    {% load static %}
    <script type="text/javascript" src='{% static 'jquery/jquery.js' %}'></script>

In production you need to call `yarn install` before `collectstatic`:

.. code-block:: bash

    ./manage.py yarn install
    ./manage.py collectstatic

If you need to pass arguments to yarn, like `--flat`, use:

.. code-block:: bash

    ./manage.py yarn install -- --flat

You can call yarn commands like `info` and `update` with:

.. code-block:: bash

    ./manage.py yarn info backbone
    ./manage.py yarn update
